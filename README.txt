// $Id$

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

Current Maintainer: Mark Davies <mark.davies@ixis.co.uk>

Uber_Invoice is a simple helper module that allows users to override the 
default Ubercart invoice templates by enabling the module and editing the 
templates contained within this module.

This helps you have invoice templates per site, if running a multi site 
install.

The module contains the following files which should not be edited.
	uc_order.tpl.php
	uc_order-admin.tpl.php
	uc_order-customer.tpl.php

These are ubercart core invoice templates and are required in the module 
folder to allow the override to work.


INSTALLATION
------------

1. Drop the module into sites/all/modules/ if your running multisite, or drop the module into 
	 your specific sites/<yoursite.com>/modules/ folder.

2. Enable the module (depends on Ubercart)

3. Clear your cache at /admin/settings/performance.

3. Your Done.


USAGE
------------

1.  The module comes with two predefined invoice templates that can be edited.
		uc_order-ubercustomer.tpl.php
		uc_order-uberadmin.tpl.php
		
		Edit these invoices as you wish and save.
		
		Additional templates can be created cloning the named files above and by then editing 
		uber_invoice_uc_invoice_templates() function in the uber_invoice.module file (See comments in .module file).
		
2.  After you've made your changes you need to select the new templates in the Ubercart conditional actions configuration 
		section to use your new templates.  These admin pages can be found at:
		
		/admin/store/ca/uc_checkout_admin_notification/edit/actions
		/admin/store/ca/uc_checkout_customer_notification/edit/actions
		
------------
		
		